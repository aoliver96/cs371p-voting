// -----------
// Voting.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <list>     // list
#include <vector>   // vector
#include <map>      // map
#include <algorithm>// find

#include "Voting.hpp"

using namespace std;

// ------
// Ballot
// ------
Ballot::Ballot(string s) {
    int temp;
    istringstream s2(s);
    while(s2 >> temp)
    {
        preferences.push_back(temp);
    }
}

// --------------
// next_candidate
// --------------
int Ballot::next_candidate() {
    if(preferences.empty())
    {
        return 0;
    }
    preferences.pop_front();
    return preferences.front();
}

// -----------------
// current_candidate
// -----------------
int Ballot::current_candidate() {
    if(preferences.empty())
    {
        return 0;
    }
    return preferences.front();
}

// -----------------
// print_preferences
// -----------------
string Ballot::print_preferences() {
    string output;
    for(list<int>::iterator i = preferences.begin(); i != preferences.end(); ++i)
    {
        output += to_string(*i) + " ";
    }
    output.erase(output.end()-1);
    return output;
}

// ---------
// Candidate
// ---------
Candidate::Candidate(string s) : name(s) {
    vector<Ballot> votes();
}

// ---------------------
// Candidate::operator==
// ---------------------
bool Candidate::operator==(const Candidate& rhs)
{
    if(this->name.compare(rhs.name) == 0)
    {
        return true;
    }
    return false;
}

// ----------
// add_ballot
// ----------
void Candidate::add_ballot(Ballot& b)
{
    votes.push_back(b);
}

// ----------
// new_ballot
// ----------
void Candidate::new_ballot(string s)
{
    votes.emplace_back(s);
}

// -------------
// count_ballots
// -------------
int Candidate::count_ballots()
{
    return votes.size();
}

// ------
// Voting
// ------
Voting::Voting()
{
    map<int, Candidate> candidates();
    num_votes = 0;
}

// -------------
// add_candidate
// -------------
void Voting::add_candidate(int k, string s)
{
    candidates.emplace(k, s);

}

// -------------
// assign_ballot
// -------------
void Voting::assign_ballot(string s)
{
    Ballot b(s);
    candidates.at(b.current_candidate()).add_ballot(b);
    ++num_votes;
}


// ----------------
// reallocate_votes
// ----------------
void Voting::reallocate_votes(Candidate& c)
{
    int next;
    for(vector<Ballot>::iterator i = c.votes.begin(); i != c.votes.end(); ++i)
    {
        do
        {
            next = (*i).next_candidate();
        } while (candidates.find(next) == candidates.end());

        candidates.at(next).add_ballot(*i);
    }
}

// ------------
// run_election
// ------------
vector<Candidate*> Voting::run_election()
{
    int current_tally, highest_tally, lowest_tally;
    vector<Candidate*> winners;
    list<int> losers;

    while(true)
    {
        current_tally = 0;
        highest_tally = 0;
        lowest_tally = 1001; //max b+1

        for(map<int, Candidate>::iterator i = candidates.begin(); i != candidates.end(); ++i)
        {
            current_tally = (*i).second.count_ballots();
            if(current_tally <= lowest_tally)
            {
                if(current_tally != lowest_tally)
                {
                    losers.clear();
                    lowest_tally = current_tally;
                }
                losers.push_back((*i).first);
            }
            if(current_tally >= highest_tally)
            {
                if(current_tally != highest_tally)
                {
                    winners.clear();
                    highest_tally = current_tally;
                }
                winners.push_back(&(*i).second);
            }
        }

        if( (num_votes % candidates.size()) == 0 && winners.size() == candidates.size())
        {
            return winners;
        }

        if(highest_tally > (num_votes/2))
        {
            return winners;
        }

        for(list<int>::iterator i = losers.begin(); i != losers.end(); ++i)
        {
            reallocate_votes(candidates.at(*i));
            candidates.erase(*i);
        }
    }
}

// -------------
// parse_problem
// -------------
Voting parse_problem(istream& sin)
{
    Voting v;
    string line;

    // Read n
    getline(sin, line);
    int candidates = stoi(line);

    // Create Candidates from names
    for(int n = 1; n<=candidates; ++n)
    {
        getline(sin, line);
        v.add_candidate(n, line);
    }

    // Create Ballot objects
    while(!line.empty())
    {
        getline(sin, line);
        if(!line.empty())
        {
            v.assign_ballot(line);
        }
    }

    return v;
}

// ------------
// voting_solve
// ------------
void voting_solve(istream& sin, ostream& sout)
{
    string line;
    int problems = 0;

    // parse problem input for number of vote instances
    getline(sin, line);
    problems = stoi(line);

    getline(sin, line);

    // run election on problem(s)
    for(; problems>0; --problems)
    {
        Voting v = parse_problem(sin);
        vector<Candidate*> solution = v.run_election();

        for(vector<Candidate*>::iterator i = solution.begin(); i != solution.end(); ++i)
        {
            sout<<(**i).name<<"\n";
        }

        // Add a dividing line for the outputs
        if(problems != 1)
        {
            sout<<"\n";
        }
    }
}
