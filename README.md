# CS371p: Object-Oriented Programming Voting Repo

* Name: Alex Oliver

* EID: AJO695

* GitLab ID: aoliver96

* HackerRank ID: oliver_alex96

* Git SHA: 10a39789184a74cd2c0d944325974bc3898a12bb

* GitLab Pipelines: https://gitlab.com/aoliver96/cs371p-voting/-/pipelines

* Estimated completion time: 20 hours

* Actual completion time: ~19 hours

* Comments:
I initially set out to follow Professor Downing's advice to do the I/O first, but I figured I'd
have it output from the processed form back into its input form. This didn't exactly go as planned
and I just ended up building the entire program logic from the bottom up, building unit tests as I
went. Not exactly the intended method, but hey, practicing test driven development all the same.

This one took longer than collatz even though I didn't run into nearly as many bugs, simply because
starting from scratch is always the hardest thing for me.

Attributions:
    - The Makefile code is primarily the Collatz makefile, adjusted to work for this project, with
        some of the unnecessary commands stripped out.
    - The CheckTestData schema is primarily based on Samuel Laberge's piazza entry, with Orion
        Reynold's mentioned comments, and a few tweaks.