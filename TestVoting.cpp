// --------------
// TestVoting.c++
// --------------

// https://github.com/google/googletest/blob/master/docs/primer.md

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Voting.hpp"

using namespace std;

// ------
// Ballot
// ------

TEST(BallotTests, ReadWrite_StringOfNumbers)
{
    Ballot test_ballot("1 2 3");
    ASSERT_EQ(test_ballot.print_preferences(), "1 2 3");
}

TEST(BallotTests, Pop_RegularInput)
{
    Ballot test_ballot("1 2 3");
    ASSERT_EQ(test_ballot.current_candidate(), 1);
    ASSERT_EQ(test_ballot.next_candidate(), 2);
}

// ---------
// Candidate
// ---------

TEST(CandidateTests, Constructor_BasicName)
{
    Candidate test_candidate("Timmy");
    ASSERT_EQ(test_candidate.name, "Timmy");
}

TEST(CandidateTests, ReadWrite_SingleBallot)
{
    Candidate test_candidate("Timmy");
    Ballot test_ballot("1 2 3");
    test_candidate.add_ballot(test_ballot);

    ASSERT_EQ(test_candidate.votes[0].current_candidate(), test_ballot.current_candidate());
    while(test_ballot.current_candidate() != 0)
    {
        ASSERT_EQ(test_candidate.votes[0].next_candidate(), test_ballot.next_candidate());
    }
}

TEST(CandidateTests, ReadWrite_ConstructBallot)
{
    Candidate test_candidate("Timmy");
    Ballot test_ballot("1 2 3");
    test_candidate.new_ballot("1 2 3");

    ASSERT_EQ(test_candidate.votes[0].current_candidate(), test_ballot.current_candidate());
    while (test_ballot.current_candidate() != 0)
    {
        ASSERT_EQ(test_candidate.votes[0].next_candidate(), test_ballot.next_candidate());
    }
}

TEST(CandidateTests, Count_TenBallots)
{
    Candidate test_candidate("Timmy");
    for(int i = 1; i<=10; ++i)
    {
        test_candidate.new_ballot(to_string(i));
    }
    ASSERT_EQ(test_candidate.count_ballots(), 10);
}

// ------
// Voting
// ------

TEST(VotingTests, ReadWrite_Candidates)
{
    Voting test_voting;
    test_voting.add_candidate(1, "Timmy");
    ASSERT_EQ(test_voting.candidates.at(1).name, "Timmy");
}

TEST(VotingTests, Reassign_ThreeCandidates_TenVotes)
{
    Voting test_voting;
    vector<Candidate> eliminated;

    test_voting.add_candidate(1, "Timmy");
    test_voting.add_candidate(2, "Jimmy");
    test_voting.add_candidate(3, "Limmy");

    for(int i = 1; i<=5; ++i)
    {
        test_voting.assign_ballot("1 2 3");
        test_voting.assign_ballot("1 3 2");
    }
    test_voting.reallocate_votes(test_voting.candidates.at(1));

    ASSERT_EQ(test_voting.candidates.at(2).count_ballots(), 5);
    ASSERT_EQ(test_voting.candidates.at(3).count_ballots(), 5);
}

TEST(VotingTests, Reassign_OneEliminated)
{
    Voting test_voting;

    test_voting.add_candidate(1, "Timmy");
    test_voting.add_candidate(2, "Jimmy");
    test_voting.add_candidate(3, "Limmy");

    test_voting.candidates.erase(2);

    for (int i = 1; i <= 5; ++i)
    {
        test_voting.assign_ballot("1 2 3");
        test_voting.assign_ballot("1 3 2");
    }
    test_voting.reallocate_votes(test_voting.candidates.at(1));

    ASSERT_EQ(test_voting.candidates.at(3).count_ballots(), 10);
}

TEST(VotingTests, Election_InstantMajority)
{
    Voting test_voting;

    test_voting.add_candidate(1, "Timmy");
    test_voting.add_candidate(2, "Jimmy");
    test_voting.add_candidate(3, "Limmy");

    test_voting.candidates.erase(2);

    for (int i = 1; i <= 10; ++i)
    {
        test_voting.assign_ballot("1 2 3");
    }

    vector<Candidate *> winner = test_voting.run_election();

    ASSERT_EQ(winner.size(), 1);
    ASSERT_EQ( (*winner.front()).name, "Timmy");
}

TEST(VotingTests, Election_SingleRunoff_SingleWinner)
{
    Voting test_voting;

    test_voting.add_candidate(1, "Timmy");
    test_voting.add_candidate(2, "Jimmy");
    test_voting.add_candidate(3, "Limmy");

    for (int i = 1; i <= 5; ++i)
    {
        test_voting.assign_ballot("1 2 3");
    }
    for (int i = 1; i <= 3; ++i)
    {
        test_voting.assign_ballot("2 3 1"); //Fails Here
        //assert(false);
    }
    for (int i = 1; i <= 2; ++i)
    {
        test_voting.assign_ballot("3 1 2");
    }

    vector<Candidate *> winner = test_voting.run_election();

    ASSERT_EQ(winner.size(), 1);
    ASSERT_EQ((*winner.front()).name, "Timmy");
}

TEST(VotingTests, Election_HR0)
{
    Voting test_voting;

    test_voting.add_candidate(1, "John Doe");
    test_voting.add_candidate(2, "Jane Smith");
    test_voting.add_candidate(3, "Sirhan Sirhan");

    test_voting.assign_ballot("1 2 3");
    test_voting.assign_ballot("2 1 3");
    test_voting.assign_ballot("2 3 1");
    test_voting.assign_ballot("1 2 3");
    test_voting.assign_ballot("3 1 2");

    vector<Candidate *> winner = test_voting.run_election();

    ASSERT_EQ(winner.size(), 1);
    ASSERT_EQ((*winner.front()).name, "John Doe");
}

TEST(VotingTests, Election_TwoWayTie)
{
    Voting test_voting;

    test_voting.add_candidate(1, "Timmy");
    test_voting.add_candidate(2, "Jimmy");
    test_voting.add_candidate(3, "Limmy");

    for (int i = 1; i <= 2; ++i)
    {
        test_voting.assign_ballot("1 2 3");
    }
    for (int i = 1; i <= 2; ++i)
    {
        test_voting.assign_ballot("2 1 3");
    }

    vector<Candidate *> winner = test_voting.run_election();

    ASSERT_EQ(winner.size(), 2);
    ASSERT_EQ((*winner[0]).name, "Timmy");
    ASSERT_EQ((*winner[1]).name, "Jimmy");
}

TEST(VotingTests, Election_AllWayTie)
{
    Voting test_voting;

    test_voting.add_candidate(1, "Timmy");
    test_voting.add_candidate(2, "Jimmy");
    test_voting.add_candidate(3, "Limmy");

    test_voting.assign_ballot("1 2 3");
    test_voting.assign_ballot("2 1 3");
    test_voting.assign_ballot("3 1 2");

    vector<Candidate *> winner = test_voting.run_election();

    ASSERT_EQ(winner.size(), 3);
    ASSERT_EQ((*winner[0]).name, "Timmy");
    ASSERT_EQ((*winner[1]).name, "Jimmy");
    ASSERT_EQ((*winner[2]).name, "Limmy");
}

// ---
// I/O
// ---

TEST(IOTests, ParseProblem)
{
    istringstream sin("2\nTimmy\nJimmy\n1 2 3\n1 3 2\n");

    Voting test_voting = parse_problem(sin);

    // assert(false);
    ASSERT_EQ(test_voting.candidates.size(), 2);
    ASSERT_EQ(test_voting.candidates.at(1).name, "Timmy");
    ASSERT_EQ(test_voting.candidates.at(2).name, "Jimmy");

    vector<Ballot> Timmy_votes = test_voting.candidates.at(1).votes;
    ASSERT_EQ(Timmy_votes[0].print_preferences(), "1 2 3");
    ASSERT_EQ(Timmy_votes[1].print_preferences(), "1 3 2");
}

TEST(IOTests, PrintSolution_OneProblem)
{
    istringstream sin("1\n\n2\nTimmy\nJimmy\n1 2 3\n1 3 2\n");
    ostringstream sout;
    voting_solve(sin, sout);

    ASSERT_EQ(sout.str(), "Timmy\n");
}

TEST(IOTests, PrintSolution_HR0)
{
    istringstream sin("1\n\n3\nJohn Doe\nJane Smith\nSirhan Sirhan\n1 2 3\n2 1 3\n2 3 1\n1 2 3\n3 1 2\n");
    ostringstream sout;
    voting_solve(sin, sout);

    ASSERT_EQ(sout.str(), "John Doe\n");
}

TEST(IOTests, PrintSolution_OneTie)
{
    istringstream sin("1\n\n2\nTimmy\nJimmy\n1 2 3\n2 3 1\n");
    ostringstream sout;
    voting_solve(sin, sout);

    ASSERT_EQ(sout.str(), "Timmy\nJimmy\n");
}

TEST(IOTests, PrintSolution_TwoProblems)
{
    istringstream sin("2\n\n2\nTimmy\nJimmy\n1 2 3\n2 3 1\n\n2\nTimmy\nJimmy\n1 2 3\n1 3 2\n");
    ostringstream sout;
    voting_solve(sin, sout);

    ASSERT_EQ(sout.str(), "Timmy\nJimmy\n\nTimmy\n");
}