// --------------
// RunVoting.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <string>

#include "Voting.hpp"

using namespace std;

// ----
// main
// ----

int main () {

    voting_solve(cin, cout);
    return 0;
}
