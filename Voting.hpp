// ---------
// Voting.h
// ---------

#ifndef Voting_h
#define Voting_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <list>     // list
#include <vector>   // vector
#include <string>   // string
#include <map>      // map

using namespace std;

// -------
// classes
// -------

// ------
// Ballot
// ------
class Ballot
{
private:
    list<int> preferences;

public:
    // ------
    // Ballot
    // ------

    /**
    * @param a string
    * @return a Ballot object
    */
    Ballot(string s);

    // --------------
    // next_candidate
    // --------------
    /**
    * Pop the first candidate off the queue and return the next preferred candidate.
    * @return an int
    */
    int next_candidate();

    // -----------------
    // current_candidate
    // -----------------
    /**
    * Return the most preferred candidate.
    * @return an int
    */
    int current_candidate();

    // -----------------
    // print_preferences
    // -----------------
    /**
    * Returns a string containing the votes in the queue, formatted like the problem input.
    * @return an int
    */
    string print_preferences();
};


// ---------
// Candidate
// ---------
class Candidate {
public:
    vector<Ballot> votes;
    const string name;

    // ---------
    // Candidate
    // ---------
    /**
    * Construcs a Candidate with name s, and an empty votes list
    * @param a string
    * @return a Candidate object
    */
    Candidate(string s);

    bool operator==(const Candidate& rhs);

    // ----------
    // add_ballot
    // ----------
    /**
    * Appends a copy of Ballot b to the set.
    * @param a Ballot
    */
    void add_ballot(Ballot& b);

    // ----------
    // new_ballot
    // ----------
    /**
    * Generates a new Ballot from s at the end of the votes list.
    * @param a string
    */
    void new_ballot(string s);

    // -------------
    // count_ballots
    // -------------
    /**
    * Return a count of Ballots.
    * @return an int
    */
    int count_ballots();
};

// ------
// Voting
// ------
class Voting {
public:
    map<int, Candidate> candidates;
    int num_votes;

    // ------
    // Voting
    // ------
    /**
    * @return a Voting object
    */
    Voting();

    // -------------
    // add_candidate
    // -------------
    /**
    * Constructs and adds a Candidate to the map at key k, with name s, and empty votes list.
    * @param an int
    * @param a string
    */
    void add_candidate(int k, string s);

    // -------------
    // assign_ballot
    // -------------
    /**
    * Creates a new Ballot from s, and assigns it to its first candidate.
    * @param a string
    */
    void assign_ballot(string s);

    // ----------------
    // reallocate_votes
    // ----------------
    /**
    * Reallocate the votes from c to their runners up.
    * @param a Candidate object
    */
    void reallocate_votes(Candidate& c);

    // ----------------
    // reallocate_votes
    // ----------------
    /**
    * Run the election process on the existing Candidates. Returns a list containing the winning candidate(s).
    * @return a vector<Candidate*>
    */
    vector<Candidate*> run_election();
};

// -------------
// parse_problem
// -------------
/**
* Parse the input for a problem, starting with n.
* @param an istream&
* @return a Voting object
*/
Voting parse_problem(istream& sin);

// ------------
// voting_solve
// ------------
/**
* Takes raw file input and outputs formatted answers
* @param an istream&
* @param an ostream&
*/
void voting_solve(istream& sin, ostream& sout);

#endif // Voting_h
